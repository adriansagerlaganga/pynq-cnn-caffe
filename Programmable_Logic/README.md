# CNN Vivado

The Vivado Block design for this project is as follows:

> ![](CNN_Vivado.png)

The main IP is `cnn_0`. It is writen in C++ and translated into VHDL via High-Level Synthesis.

> Vivado® High-Level Synthesis included as a no cost upgrade in all Vivado HLx Editions, accelerates IP creation by enabling C, C++ and System C specifications to be directly targeted into Xilinx programmable devices without the need to manually create RTL[⁽¹⁾](https://www.xilinx.com/products/design-tools/vivado/integration/esl-design.html).

* To modify it, create a new *HLS* project and source the files in `HLS/CNN_CORE/`.
