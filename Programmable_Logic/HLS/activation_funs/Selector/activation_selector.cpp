#include <stdio.h>
#define data_type float
#include <cmath>

void a_funs(char selector, data_type input, data_type& output)
{

#pragma HLS INTERFACE s_axilite port=output
#pragma HLS INTERFACE s_axilite port=input
#pragma HLS INTERFACE s_axilite port=selector

	enum funs {sigmoid, relu, tanh_s};

	switch(selector)
	{
		case sigmoid:
			if(input <= 0)
			{
				output =  pow((data_type) 2,input)/2;
			}
			else
			{
				output =  (1 - 1/(2*pow(2,input)));
			}
			break;
		case relu:
			if(input > 0)
			{
				output =  input;
			}
			else
			{
				output =  0;
			}
			break;
		case tanh_s:
			if(input <= 1 && input >= -1)
			{
				output =  input;
			}
			else if( input < -1)
			{
				output = -1;
			}
			else
			{
				output =  1;
			}
			break;
	}

}
