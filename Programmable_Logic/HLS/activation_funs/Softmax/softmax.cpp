#include "softmax.h"

var_data softmax(var_data matrix[vector_len], index_size_type index, index_size_type size)
{

	var_data sum = 0.0;
	for(int i = 0; i < size; i++){
		sum += exp(matrix[i]);
	}
	return expf(matrix[index])/sum;
}
