#include "math.h"

#define vector_len 5
#define var_data float
#define index_size_type short int

var_data softmax(var_data matrix[vector_len], index_size_type index, index_size_type size);
