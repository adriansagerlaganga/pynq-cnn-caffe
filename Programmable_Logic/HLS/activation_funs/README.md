# Activation Functions

The .cpp files that were used for synthetizing the IP cores are given alongside this README, their description is as follows:

## Selector

The file 'activation_selector.cpp' is a collection of various approximated activation functions whose calculation is only dependent on the current value of the activation, that is, no computation is performed using the activation of other neurons contained in that layer. Further information on their mathematical function can be found [here](https://pdfs.semanticscholar.org/4113/1b9524124075c42ec709ba6b72538e98b1b1.pdf)

## Softmax

The file 'softmax.cpp' is the exact softmax function, opposite to the previous one, it does need the values of the activations of the neurons contained in that same layer in order to perform a normalization.

## Usage

The code can be imported as-is as a the sources of a new file in Vivado HLS.
