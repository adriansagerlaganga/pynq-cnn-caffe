


#ifndef _CONV_H
#define _CONV_H

#include "hls_stream.h"
#include "data.h"
#include "layer_meta.hpp"
#include "neuron.h"

void conv(
		hls::stream<sdata>& DMA_in,
		hls::stream<sdata>& DMA_out,
		const LAYER_META md,
		neuron window[KERNEL_MAX_DIM][KERNEL_MAX_DIM]);

#endif
