/*
 * TOP LEVEL - CNN HEADER
 * */

#ifndef _CNN_H
#define _CNN_H

#include "hls_stream.h"
#include "data.h"

void cnn_0_5(
		 hls::stream<sdata>& DMA_in,
		 hls::stream<sdata>& DMA_out/*,
		 ap_uint<LAYER_TYPES_BIT> *typeOut,
		 ap_uint<N_INPUT_BIT> *n_inputOut*/);

#endif
