/*
 * Layer metadata
 *
 * Attributes
 * ----------
 * type : enum
 * 	CONV, POOL, RELU
 * n_input : uint
 * 	number of inputs = number of channels
 * n_kernel : uint
 * 	number of kernels = number of outputs = number of bias
 * dim_kernel : uint
 * 	width and height of the kernels
 * pad : uint
 * 	left, right, up, down padding for the input matrices
 * */

#ifndef _LAYER_META_H
#define _LAYER_META_H

#include "hls_stream.h"
#include "ap_axi_sdata.h"
#include "data.h"

// DEPRECATED
// 2-bit -> 4 types
#define LAYER_TYPES_BIT 2
// 8-bit -> 256 inputs
#define N_INPUT_BIT 8
// 16-bit -> 65536 width
#define W_INPUT_BIT 16
// 16-bit -> 65536 height
#define H_INPUT_BIT 16
// 8-bit -> 256 kernels
#define N_KERNEL_BIT 8
// n-bit -> 2^n width/height
#define KERNEL_DIM_BIT 6
#define KERNEL_MAX_DIM 64
// 4-bit -> 16 pad
#define PAD_BIT 4
//------------

class LAYER_META {
public:
	enum TYPE{CONV, POOL, RELU};

	unsigned type;
	unsigned n_input; // also n_channel Number of Channels
	unsigned n_kernel;
	unsigned dim_kernel;
	unsigned pad;

	// Constructor
	// Metadata comes from DRAM
	LAYER_META( hls::stream<sdata>& DRAM_in, hls::stream<sdata>& DRAM_out ) {
		sdata valIn;

		// we get the information one by one.
		// we truncate the input data according to the amount of bits we need.
		valIn = DRAM_in.read();
		type = (unsigned) valIn.data(LAYER_TYPES_BIT-1,0);
		DRAM_out.write(valIn);

		valIn = DRAM_in.read();
		n_input = (unsigned) valIn.data(N_INPUT_BIT-1,0);
		DRAM_out.write(valIn);

		valIn = DRAM_in.read();
		n_kernel = (unsigned) valIn.data(N_KERNEL_BIT-1,0);
		DRAM_out.write(valIn);

		valIn = DRAM_in.read();
		dim_kernel = (unsigned) valIn.data(KERNEL_DIM_BIT-1,0);
		DRAM_out.write(valIn);

		valIn = DRAM_in.read();
		pad = (unsigned) valIn.data(PAD_BIT-1,0);
		DRAM_out.write(valIn);
	}
};

#endif
