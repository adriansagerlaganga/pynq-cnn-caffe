#include <stdio.h>
#include "a_funs.h"
#include "hls_math.h"
#include "data.h"

//The stream for selecting the correct necessary activation function looks as follows:
// A_funs n_inputs type DATA
// 2 32 [0|1|2] 1 8 7 5 2 6 7

void a_funs(hls::stream<sdata>& DMA_in, hls::stream<sdata>& DMA_out)
{

	/*#pragma HLS INTERFACE ap_ctrl_none port=return
	#pragma HLS INTERFACE axis port=DMA_in
	#pragma HLS INTERFACE axis port=DMA_out*/

	sdata valIn = DMA_in.read();
	/*static*/ selector_type selector = (selector_type) valIn.data;
	DMA_out.write(valIn);
	valIn = DMA_in.read();
	unsigned amount = (unsigned) valIn.data;
	DMA_out.write(valIn);
	unsigned idx;

	sdata input, output;

	data_type result = 0;

	data_type sigmoid = 0;
	data_type relu = 1.0;
	data_type tanh_s = 2.0;

	if(selector == sigmoid)
	{
		loopyloop1: for(idx = 0; idx < amount; idx++){
			input = DMA_in.read();
			if(input.data <= -2)
			{
				result = 0.0;
			}
			else if(input.data >= 2)
			{
				result =  1.0;
			}
			else
			{
				result = (0.5 + (input.data/4));
			}
			output.data = result;
			output.keep = input.keep;
			output.last = input.last;
			DMA_out.write(output);
		}
	}
	else if(selector == relu)
	{
		loopyloop2: for(idx = 0; idx < amount; idx++){

			input = DMA_in.read();
			if(input.data > 0.0)
			{
				result =  input.data;
			}
			else
			{
				result =  0.0;
			}
			output.data = result;
			output.keep = input.keep;
			output.last = input.last;
			DMA_out.write(output);
		}
	}
	else if(tanh_s == selector)
	{
		loopyloop3: for(idx = 0; idx < amount; idx++){

			input = DMA_in.read();
			if(input.data <= 1.0 && input.data >= -1.0)
			{
				result =  input.data;
			}
			else if(input.data < -1.0)
			{
				result = -1.0;
			}
			else
			{
				result =  1.0;
			}
			output.data = result;
			output.last = input.last;
			output.keep = input.keep;
			DMA_out.write(output);
		}
	}
	else
	{
		output.data = -1.0;
		output.last = 1;
		DMA_out.write(output);
	}


	return;
}


