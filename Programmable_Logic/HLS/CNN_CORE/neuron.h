/*
 * A neuron in this context is c pieces of data,
 * where c is the number of channels.
 *
 * -- DEPRECATED
 * In the FPGA the neurons will be stored in the
 * BRAM for fast memory access.
 * --
 * */

#ifndef _NEURON_H
#define _NEURON_H


#include "hls_stream.h"
#include "ap_int.h"
#include "data.h"
#include "layer_meta.hpp"


// max number of channels: 2e4
#define CHANNEL_MAX 32
#define N_INPUT_BIT 8

struct neuron {
public:
	data_type N[CHANNEL_MAX];

	// recieves the neuron = the c inputs of a position in the matrix
	void get_neuron
# if DEBUG
	(hls::stream<sdata>& DMA_in,hls::stream<sdata>& DMA_out,unsigned  md)
#else
	(hls::stream<sdata>& DMA_in,unsigned  md)
#endif
	 {
		unsigned idx;
		sdata valIn;
		for(idx=0; idx<md; ++idx) {
		#pragma HLS PIPELINE II=1
		//#pragma HLS UNROLL

			valIn = DMA_in.read();
			N[idx] = (data_type) valIn.data;
			#if DEBUG
				DMA_out.write(valIn);
			#endif
		}
	}


	// multiplies the neuron by the retrieved kernel element (weights) and adds bias
	data_type multiply_add
#if DEBUG
	(hls::stream<sdata> & DMA_in, hls::stream<sdata> & DMA_out, data_type  md)
#else
	(hls::stream<sdata> & DMA_in, data_type  md)
#endif
	 {
		// multiply neuron
		unsigned idx;
		sdata valIn;
		data_type sum = 0;
		// for each channel
		for(idx=0; idx<md; ++idx) {
			valIn = DMA_in.read();
			sum += ((data_type)valIn.data)*N[idx];
			#if DEBUG
				DMA_out.write(valIn);
			#endif
		}
		return sum;
	}
};


#endif
