#include <hls_stream.h>
#include <ap_axi_sdata.h>
#include "data.h"
#include "layer_meta.hpp"
#include "cnn.hpp"

int main()
{

	hls::stream<sdata> DMA_cin;
	hls::stream<sdata> DMA_cout;

	sdata d;

	d.data = 1;
	DMA_cin << d;

	d.data = 0;
	DMA_cin << d;

	d.data = 1;
	DMA_cin << d;

	d.data = 3;
	DMA_cin << d;

	d.data = 1;
	DMA_cin << d;

	cnn(DMA_cin, DMA_cout);

	for(int i = 0; i < 1; i++)
	{
		std::cout << DMA_cout.read().data << std::endl;
	}

	return 0;
}
