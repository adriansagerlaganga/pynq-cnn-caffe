/*
 * Main code of the program
 * */
#include "cnn.hpp"
#include "hls_stream.h"
#include "neuron.h"
#include "data.h"
#include "layer_meta.hpp"
#include "pool.h"
#include "conv.h"
#include "a_funs.h"

#define DEBUG_LOCAL 1
#define MAX_D 16

#if DATATYPE_UNSIGNED

	//unsigned LAYER_META::type = 0;
data_type LAYER_META::n_input = 0;
data_type LAYER_META::n_kernel = 0;
data_type LAYER_META::dim_kernel = 0;
data_type LAYER_META::pad = 0;
	#if STATIC_DIM
data_type LAYER_META::w_input = 0;
data_type LAYER_META::h_input = 0;
	#endif
#else
	//ap_uint<LAYER_TYPES_BIT> LAYER_META::type = 0;
	ap_uint<N_INPUT_BIT> LAYER_META::n_input = 0;
	ap_uint<N_KERNEL_BIT> LAYER_META::n_kernel = 0;
	ap_uint<KERNEL_DIM_BIT> LAYER_META::dim_kernel = 0;
	ap_uint<PAD_BIT> LAYER_META::pad = 0;
	#if STATIC_DIM
		ap_uint<W_INPUT_BIT> LAYER_META::w_input = 0;
		ap_uint<H_INPUT_BIT> LAYER_META::h_input = 0;
	#endif

#endif


//Pooling function placeholder (Remove when the actual function is added)

//void pooling(hls::stream<sdata>& DMA_in, hls::stream<sdata>& DMA_out);

// TOP LEVEL - CNN
void cnn1_0(
		 hls::stream<sdata>& DMA_in,
		 hls::stream<sdata>& DMA_out/*,
		 ap_uint<LAYER_TYPES_BIT> *typeOut,
		 ap_uint<N_INPUT_BIT> *n_inputOut*/) {

#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=DMA_in
#pragma HLS INTERFACE axis port=DMA_out

	//Initialization of a register that will contain the value of the selection
	sdata valIn = DMA_in.read();
	/*static*/ data_type type = valIn.data;
	DMA_out.write(valIn);

	#if DEBUG_LOCAL
		static sdata temp;
		temp.data = 0;
	#endif

	//Scuffed enum definition to match float datatype
	data_type CONV = 0.0;
	data_type POOL = 1.0;
	data_type AFUNS = 2.0;

	neuron window[KERNEL_MAX_DIM][KERNEL_MAX_DIM];
#pragma HLS ARRAY_PARTITION variable=window block factor=16 dim=2
	//0 states for conv ,1 states for pooling
	if(type==CONV || type==POOL){

		#if DEBUG
			static LAYER_META metadata(DMA_in, DMA_out);
		#else
			static LAYER_META metadata(DMA_in);
		#endif

		unsigned i,j,k;
		for(i=0; i<KERNEL_MAX_DIM; i++) {
			for(j=0; j<KERNEL_MAX_DIM; j++) { //Check if this is not a problem
			//#pragma HLS UNROLL
			//the data should be sent as an array
			//#pragma HLS LATENCY min=6
			//#pragma HLS PIPELINE II=1
			if(i<metadata.dim_kernel && j<metadata.dim_kernel){
				#if DEBUG
					window[i][j].get_neuron(DMA_in,DMA_out, (unsigned) metadata.n_input);
				#else
					window[i][j].get_neuron(DMA_in,(unsigned) metadata.n_input);
				#endif

				}
			}
		}

			//Selector
		if(type == CONV)
		{
			conv(DMA_in, DMA_out, metadata, window);

		}
		else if(POOL == type)
		{
			pool(DMA_in,DMA_out,metadata,window);

		}
	}

	else if(AFUNS == type)
	{
		a_funs(DMA_in, DMA_out);

	}
	else
	{
		#if DEBUG_LOCAL
			temp.data = 255;
		#endif
	}
	//End selector

	return;
}

