/*
 * Layer metadata
 *
 * Attributes
 * ----------
 * type : enum
 * 	CONV, POOL, RELU
 * n_input : uint
 * 	number of inputs = number of channels
 * --TODO: DEPRECATED
 * w_input : uint
 * 	matrix width of input
 * h_input : uint
 * 	matrix height of input
 * --
 * n_kernel : uint
 * 	number of kernels = number of outputs = number of bias
 * dim_kernel : uint
 * 	width and height of the kernels
 * pad : uint
 * 	left, right, up, down padding for the input matrices
 * */

#ifndef _LAYER_META_H
#define _LAYER_META_H

#include "hls_stream.h"
#include "ap_axi_sdata.h"
#include "data.h"

#define DEBUG 1
#define DATATYPE_UNSIGNED 1
#define STATIC_DIM 0

// 2-bit -> 4 types
#define LAYER_TYPES_BIT 2
// 8-bit -> 256 inputs
#define N_INPUT_BIT 8
// 16-bit -> 65536 width
#define W_INPUT_BIT 16
// 16-bit -> 65536 height
#define H_INPUT_BIT 16
// 8-bit -> 256 kernels
#define N_KERNEL_BIT 8
#define N_KERNEL_MAX 256
// n-bit -> 2^n width/height
#define KERNEL_DIM_BIT 6
#define KERNEL_MAX_DIM 64
// 4-bit -> 16 pad
#define PAD_BIT 4

class LAYER_META {
public:


	//Using directives to simplify whenever it is needed to change the data type approach (Just change DATATYPE_UNSIGNED to 1 if unsigned is needed)
/*
#if DATATYPE_UNSIGNED
	static unsigned type;
#else
	static ap_uint<LAYER_TYPES_BIT> type;
#endif*/

#if DATATYPE_UNSIGNED
	static data_type n_input; // also n_channel Number of Channels
#else
	static ap_uint<N_INPUT_BIT> n_input;
#endif

//Set STATIC_DIM to 0 if the approach is that of a moving window
#if STATIC_DIM
	#if DATATYPE_UNSIGNED
		static data_type w_input;
	#else
		static ap_uint<W_INPUT_BIT> w_input;
	#endif
	#if DATATYPE_UNSIGNED
		static data_type h_input;
	#else
		static ap_uint<H_INPUT_BIT> h_input;
	#endif
#endif

	//--s
#if DATATYPE_UNSIGNED
	static data_type n_kernel; // also n_channel Number of Channels
#else
	static ap_uint<N_KERNEL_BIT> n_kernel;
#endif

#if DATATYPE_UNSIGNED
	static data_type dim_kernel; // also n_channel Number of Channels
#else
	static ap_uint<KERNEL_DIM_BIT> dim_kernel;
#endif

#if DATATYPE_UNSIGNED
	static data_type pad; // also n_channel Number of Channels
#else
	static ap_uint<PAD_BIT> pad;
#endif


	// Constructor
	// Metadata comes from DRAM
	LAYER_META
	#if DEBUG
		(hls::stream<sdata>& DMA_in, hls::stream<sdata>& DMA_out)
	#else
		(hls::stream<sdata>& DMA_in)
	#endif
		{
		#if DEBUG
				sdata valIn, valOut;
		#else
				sdata valIn;
		#endif

		// we get the information one by one.
		// we truncate the input data according to the amount of bits we need.

		//Start n_input

		valIn = DMA_in.read();

		#if DATATYPE_UNSIGNED
				n_input = (data_type) valIn.data;
		#else
				n_input = (ap_uint<N_INPUT_BIT>) valIn.data;
		#endif

		#if DEBUG
			DMA_out.write(valIn);
		#endif

		//End n_input

		//Start w_input and h_input (If Static_Dim is 1)

		#if STATIC_DIM
			valIn = DMA_in.read();

			#if DATATYPE_UNSIGNED
				w_input = (data_type) valIn.data;
			#else
					w_input = (ap_uint<W_INPUT_BIT>) valIn.data;
			#endif

			#if DEBUG
				DMA_out.write(valIn);
			#endif

			valIn = DMA_in.read();
			#if DATATYPE_UNSIGNED
					h_input = (data_type) valIn.data;
			#else
					h_input = (ap_uint<H_INPUT_BIT>) valIn.data;
			#endif

			#if DEBUG
				DMA_out.write(valIn);
			#endif

		#endif

		//End w_input and h_input (If Static_Dim is 1)

		//Start n_kernel

		valIn = DMA_in.read();

		#if DATATYPE_UNSIGNED
			n_kernel = (data_type) valIn.data;
		#else
			n_kernel = (ap_uint<N_KERNEL_BIT>) valIn.data(N_KERNEL_BIT-1,0);
		#endif

		#if DEBUG
			DMA_out.write(valIn);
		#endif

		//End n_kernel

		//Start dim_kernel

		valIn = DMA_in.read();

		#if DATATYPE_UNSIGNED
		dim_kernel = (data_type) valIn.data;
		#else
			dim_kernel = (ap_uint<KERNEL_DIM_BIT>) valIn.data(KERNEL_DIM_BIT-1,0);
		#endif

		#if DEBUG
			DMA_out.write(valIn);
		#endif


		//End dim_kernel

		//Start pad

		valIn = DMA_in.read();

		#if DATATYPE_UNSIGNED
			pad = (data_type) valIn.data;
		#else
			pad = (ap_uint<PAD_BIT>) valIn.data(PAD_BIT-1,0);
		#endif

		#if DEBUG
			DMA_out.write(valIn);
		#endif

		//End pad
/*
		//Start type

		#if DATATYPE_UNSIGNED
				type = (unsigned) valIn.data(LAYER_TYPES_BIT-1,0);
		#else
				type = (ap_int<LAYER_TYPES_BIT>) valIn.data(LAYER_TYPES_BIT-1,0);
		#endif

		#if DEBUG
			DMA_out.write(valIn);
		#endif

		//End type*/

	/*#if DEBUG
		sdata valOut;

		for(unsigned idx = 0; idx < 1; ++idx)
				{
			#pragma HLS PIPELINE II=1
				//valIn = DMA_in.read();
				valOut.data = valIn.data;
				valOut.keep = valIn.keep;
				valOut.last = 1;
				DMA_out.write(valOut);

			}
	#endif*/
	}
};

#endif
