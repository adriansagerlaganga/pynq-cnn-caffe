/*
 * TOP LEVEL - CNN HEADER
 * */

#ifndef _CNN_H
#define _CNN_H

#include "hls_stream.h"
#include "data.h"

void cnn(
		 hls::stream<sdata>& DMA_in,
		 hls::stream<sdata>& DMA_out
		 );

#endif
