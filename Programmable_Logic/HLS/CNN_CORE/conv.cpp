/*
 * Kernel multiplication
 */
#include "conv.h"
#include "hls_stream.h"
#include "data.h"
#include "layer_meta.hpp"
#include "neuron.h"

void conv(
		hls::stream<sdata>& DMA_in,
		hls::stream<sdata>& DMA_out,
		const LAYER_META md,
		neuron window[KERNEL_MAX_DIM][KERNEL_MAX_DIM]
		)
{

	// now we get all the neurons of the window:
	unsigned i,j,o;
	sdata valIn;
	sdata valOut;
	// output element
	data_type output;
	// bias
	data_type bias;
	// we calculate the kernel multiplication
	// for all the possible outputs
	for(o=0; o<(unsigned)N_KERNEL_MAX; ++o){
		if(o<(unsigned)md.n_kernel) {
			output = 0;
			// all elements of the window/kernel (all weights)
			for(i=0; i<(unsigned)KERNEL_MAX_DIM; ++i)
				for(j=0; j<(unsigned)KERNEL_MAX_DIM; ++j)
				{
					if(i<(unsigned)md.dim_kernel && j<(unsigned)md.dim_kernel)
					// multiply kernel element with neuron
					#if DEBUG
						output += window[i][j].multiply_add(DMA_in,DMA_out, md.n_input);
					#else
						output += window[i][j].multiply_add(DMA_in, md.n_input);
					#endif

				}
			// bias
			valIn = DMA_in.read();
			bias = (data_type)valIn.data;

			// sum to output and send
			output += bias;

			valOut.data = output;
			valOut.last = o == md.n_kernel-1;
			valOut.keep = valIn.keep;
			DMA_out.write(valOut);
		}
	}

}
