#ifndef _DATA_H
#define _DATA_H

#include "ap_int.h"

//Vanilla ap_axis doesn't work with float values, this template makes sure such implementation is allowed

typedef float data_type;

template<int D>
  struct ap_axis_custom{
	data_type       data;
    ap_uint<(D+7)/8> keep;
    ap_uint<1>       last;

 };

typedef ap_axis_custom<32> sdata;

#endif
