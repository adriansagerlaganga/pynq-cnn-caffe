#include "hls_stream.h"
#include "data.h"
#include "neuron.h"
#include "layer_meta.hpp"

void pool(
		hls::stream<sdata>& DMA_in,
		hls::stream<sdata>& DMA_out,
		LAYER_META md,
        neuron input[KERNEL_MAX_DIM][KERNEL_MAX_DIM]
		){

	//Reading pool type

	data_type pool_type;
	sdata valIn = DMA_in.read();
	pool_type = valIn.data;
	#if DEBUG
		DMA_out.write(valIn);
	#endif

	//End of reading pool type

	//0-max 1-avr
	//works with symmetric even functions
	//Reading of the stride size (s)
	unsigned s;
	valIn = DMA_in.read();
	s = (unsigned) valIn.data;
	#if DEBUG
		DMA_out.write(valIn);
	#endif

	data_type max = 0;
	data_type avr;
	data_type value = 0;
	sdata output;
	//i rows, j columns
	unsigned i, j, k, z, c;


	for (i = 0; i < (unsigned)KERNEL_MAX_DIM; i += s) {

		for (j = 0; j < (unsigned)KERNEL_MAX_DIM; j += s) {
			if(i<md.dim_kernel && j<md.dim_kernel) {
				max = 0.0;
				avr= 0.0;
				for(c=0; c < (unsigned) md.n_input; ++c) {
					for (k = 0; k < (unsigned)KERNEL_MAX_DIM; k++) {
						for (z = 0; z < (unsigned)KERNEL_MAX_DIM; z++) {
							if(k < md.dim_kernel && z < md.dim_kernel) {
								value = input[i + k][j + z].N[c];
								avr+=value;
								max = value > max ? value : max;
							}
						}
					}

				}
				output = DMA_in.read();
				if(pool_type==0)
				{
					output.data = (data_type)max;
				}
				else{
					avr=avr/(data_type)(md.dim_kernel*md.dim_kernel);
					output.data = (data_type)avr;
				}
				output.last = (i==md.dim_kernel-1 && j==md.dim_kernel-1);
				DMA_out.write(output);
			}
		}
	}

	return;
}
