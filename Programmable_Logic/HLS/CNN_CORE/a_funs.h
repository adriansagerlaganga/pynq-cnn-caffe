#ifndef A_FUNS_DEF
#define A_FUNS_DEF

#include "data.h"
#include "hls_stream.h"

typedef ap_uint<2> selector_type;

void a_funs(hls::stream<sdata>& DMA_in, hls::stream<sdata>& DMA_out);

#endif
