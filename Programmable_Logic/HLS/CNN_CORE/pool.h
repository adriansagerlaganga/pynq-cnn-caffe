#ifndef _POOL_H
#define _POOL_H

#include "hls_stream.h"
#include "data.h"
#include "neuron.h"

void pool(
		hls::stream<sdata>& DMA_in,
		hls::stream<sdata>& DMA_out,
		LAYER_META md,
        neuron input[KERNEL_MAX_DIM][KERNEL_MAX_DIM]
		);

#endif
