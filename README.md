# Convolutional Neural Network implemented with PYNQ-z2 board

General approach to implement CNNs in a FPGA using PYNQ-z2, following LeNet-5 example in Caffe framework.

---

## Table of contents

- [What's a convolutional neural network (CNN)?](#whats-a-convolutional-neural-network-cnn)
	- [Convolution](#convolution)
	- [Pooling Layer](#pooling-layer)
	- [Activation Layer](#activation-layer)
- [CAFFE](#caffe)
- [Setup and Implementation](#setup-and-implementation)
- [Report](#report)

---
## What's a convolutional neural network (CNN)?
> In deep learning, a convolutional neural network (CNN, or ConvNet) is a class of deep neural networks, most commonly applied to analyzing visual imagery.[(*)](https://en.wikipedia.org/wiki/Convolutional_neural_network)

Convolutional neural network is used to find patterns in an image.
This is done by convoluting over an image and looking for patterns.

Paper explaining feed-forward networks in a hardware context: [**link**](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&uact=8&ved=2ahUKEwjW56bSgdvhAhVGyqQKHc7ED2AQFjADegQIAhAC&url=http%3A%2F%2Fciteseerx.ist.psu.edu%2Fviewdoc%2Fdownload%3Fdoi%3D10.1.1.18.593%26rep%3Drep1%26type%3Dpdf&usg=AOvVaw00hfDznxGN5Oro58etmtrq).

> ![Feed-forward network diagram. Each connection has a weight.](https://codeplea.com/public/content/genann_simple.png) <br>
> The most basic description of a neural network is a weighted graph

> <h5> LeNet-5 </h5>
>
> ![](https://miro.medium.com/max/1000/1*1TI1aGBZ4dybR6__DI9dzA.png) <br>
> Article: [link](https://medium.com/@pechyonkin/key-deep-learning-architectures-lenet-5-6fc3c59e6f4) <br>
> Original paper, [[LeCun et al.,1998]](http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf)

It contains various layers of which the most important for implementation are **convolutional layer**, **pooling layer**, **activation layer**.

### Convolution

The goal of a convolutional layer is **filtering**.

As we move over an image we effectively check for patterns in that section of the image. This works because

of **filters** (also called *kernels*), stacks of weights represented as a vector, which are multiplied by the values outputed by the convolution.

When training an image, these weights
change, and so when it is time to evaluate an image, these weights return high
values if it thinks it is seeing a pattern it has seen before. The combinations of high
weights from various filters let the network predict the content of an image.

### Pooling Layer

The layer after convolutional layer is mostly pooling layer in CNN architecture. It
partitions the input image into a set of non-overlapping rectangles and, for each
such sub-region, outputs a value.

> The intuition is that the exact location of a
feature is less important than its rough location relative to other features.

The two main pooling layers are **max-pooling** and **average pooling**.

### Activation Layer

Activation function is a node that is put at the end of or in between Neural Networks. They help to decide if the neuron would fire or not.

Check [this article](https://www.analyticsvidhya.com/blog/2017/10/fundamentals-deep-learning-activation-functions-when-to-use-them/) for a description on activation layers.

# CAFFE
> **Caffe** is a framework for *deep learning* that provides CNNs from multiple papers in their *model zoo*: [**link**](https://github.com/BVLC/caffe/wiki/Model-Zoo).

* This CNNs come in two type files:
    * `'*.prototxt'` (containing the model info) and `'*.caffemodel'` (containing weight and bias info).

# Setup and Implementation
Please check the folders `Processing_System/` and `Programmable_Logic/` for more information on how to setup this project and how it is implemented.

# Report
You can check out the report of this project on the following [link](https://drive.google.com/file/d/1nB3WeziMTvNOR97jnKXeyV1FTrk8xUzp/view?usp=sharing).
