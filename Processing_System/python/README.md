
### Configuration file

Comes in a `.json` format [⁽¹⁾](https://www.json.org/), in the file `PL_conf.json`.

Includes the following:

###### Default Bitstream

> An FPGA bitstream is a file that contains the programming information for an FPGA [⁽¹⁾](https://www.xilinx.com/html_docs/xilinx2018_1/SDK_Doc/SDK_concepts/concept_fpgabitstream.html).

Comes in the folder `/home/xilinx/CNN/overlay/`.

###### Default CNN Model

> In deep learning, a convolutional neural network (CNN, or ConvNet) is a class of deep neural networks, most commonly applied to analyzing visual imagery [⁽¹⁾](https://en.wikipedia.org/wiki/Convolutional_neural_network).

Comes in the folder `/home/xilinx/CNN/models/default.caffemodel` and `/home/xilinx/CNN/models/default.prototxt`.

### Code

The python code of the library acts as a driver between the **PS** and **PL**.

To do that, heavy bit manipulation is required. A helpful tutorial on bit manipulation
on python is found [online](https://wiki.python.org/moin/BitManipulation).

## Print CNN Layer

Shows weights and bias of a given layer of a CNN, or the 1st one as default.

#### Usage
- Open your terminal and run the file `print_layers.py` followed by the name of the CNN

```shell
root@root:~/pynq-cnn-caffe/Processing_System/python$ python3 print_layers.py model_name [layer_name]
```

> You should have a `<CNN>.prototxt` and `<CNN>.caffemodel` in the same folder.
