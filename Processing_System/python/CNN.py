import os
import sys

import math
import json
sys.path.insert(0, "/home/xilinx/caffe/python/") #Location of the pycaffe libraries.
import caffe
#To gather kernel/layer information
from caffe.proto import caffe_pb2
from google.protobuf import text_format

import numpy as np
from pynq import Overlay
# Used for contiguous memory allocation (CMA)
# Which means, allocating physical memory
# in order so that the DMA can access it
# contiguously
from pynq import Xlnk

# metadata from json
_metadata = None
with open('PL_conf.json') as json_file:
    _metadata = json.load(json_file)

"""
We have to use Xlnk to store the data in DRAM, so the DMA in the
PS is able to access it

There can be multiple input layers!!
Because of this, the _Driver class has a list of neurons for
each input layer.
We should then execute the whole CNN for each input.
"""
xlnk = Xlnk()

class _Driver:
    """
    Manages the connection between the PS and PL

    We have to use Xlnk to store the data in DRAM, so the DMA in the
    PS is able to access it

    Parameters
    ----------
    ovname : String
        name of the Overlay.

    Attributes
    ----------
    overlay : a :class:`Overlay` instance
        FPGA overlay
    net : a :class:`Net` instance
        is the Caffe Network itself
    net_proto : a :class:`Net` instance
        is the proto data of the Caffe Network
    window : a :class:`np.ndarray` instance
        goes through the neurons
    neurons : a :class:`dict` instance
        for each possible input, lists all the
        neurons of the input matrix. The keys are the
        names of the input layers.
        Each neuron is a :class:`numpy.ndarray` instance
        with tuple shape (Nchannels, Width, Height)
    """
    def __init__(self, ovname):
        # Initialize the Overlay
        self.overlay = Overlay(ovname)
        self.dma = self.overlay.axi_dma_0
        self.net = None
        self.net_proto = None
        self.window = None
        self.neurons = None
        self.dma_array = None

    def init_model(self, model_name):
        """
        Parameters
        ----------
        model_name : String
            path of the .caffemodel.

        """
        caffemodel_path = model_name
        prototxt_path   = caffemodel_path.replace('caffemodel', 'prototxt')
        self.net = caffe.Net(prototxt_path, caffemodel_path, caffe.TEST)
        self.net_proto = caffe_pb2.NetParameter()
        text_format.Merge(open(prototxt_path).read(), self.net_proto)
        """
        Printing the Blob Data
        -----------------------
        print('LAYERS SHAPE: ')
        for k, v in self.net.blobs.items():
            print(k, v.data.shape)
        """
        """
        Printing the Layer Data
        -----------------------
        for layer in self.net_proto.layer:
            print(layer)
        """

    def load_input(self, data):
        """
        Parameters
        ----------
        data : numpy.ndarray or list
            the data, for ex. an img or list of imgs,
            to send to the FPGA
        """
        # If we have only 1 data,
        if type(data) is np.ndarray:
            #if it's an invalid numpy array:
            if len(data.shape) < 3 or len(data.shape) > 4:
                raise ValueError('Invalid Data inserted.')
            #if the user for some reason also specified the input data
            #repeats n times (first parameter)
            if len(data.shape) == 3:
                #trasform into array
                data = [data for _ in range(len(self.net.inputs))]

        if type(data) is not list:
            raise ValueError('Given data should be a list. It is: ',type(data))

        if len(data) == 0:
            raise ValueError('Given data should have at least 1 value')

        if len(data) < len(self.net.inputs):
            dlen = len(data)
            for i in range(dlen,len(self.net.inputs)):
                data[i] = data[i%(dlen)]

        # Each input layer corresponds to an "image"
        # For every input layer:
        self.neurons = {}
        i = 0
        for input_layer_name in self.net.inputs:
            input_layer_shape = self.net.blobs[input_layer_name].data.shape
            # input_layer_shape : (Nsamples, Nchannels, Width, Height)
            # input_layer_shape[1:3] : (Nchannels, Width, Height)
            if data[i].shape != input_layer_shape[1:4]:
                raise ValueError('Data had this shape: ',data[i].shape,' It should have this shape (Nchannels, Width, Height): ',input_layer_shape[1:4])
            print("[INFO] Data inserted has the correct shape: ", input_layer_shape[1:4])
            #deep copy!
            self.neurons[input_layer_name] = data[i].copy()
            ++i

        print("NUMBER OF INPUTS: ", len(self.neurons))
        # TODO: Continue function

    def iter_neurons(self, neurons, param):
        """
        Generator function of windows.

        The windows will have the same dimensions as the
        kernels of the layer, and the same channels as the
        neurons.

        Parameters
        ----------
        neurons : numpy.ndarray
            the neurons we want to get a window from.
        param : dict
            kernel and layer information
            kernel_size, pad, stride
        """
        if type(neurons) is not np.ndarray:
            raise ValueError('Invalid type. Type: ',type(neurons))

        ks = param["kernel_size"]
        nc = neurons.shape[0]
        nw = neurons.shape[1]
        nh = neurons.shape[2]

        window = np.zeros(shape=(nc,ks,ks))
        for i in range(-param["pad"], nw-1+param["pad"], param["stride"]):
            for j in range(-param["pad"], nh-1+param["pad"], param["stride"]):
                l = max(i,0)
                r = min(nw,i+ks)-1
                u = max(j,0)
                d = min(nh,j+ks)-1
                if r-i>0 and d-j>0:
                    window[:, l-i:r-i +1, u-j:d-j +1] = neurons[:, l:r +1, u:d +1]
                #0-padding
                if i+ks > nw:
                    window[:, (nw-1)-(i):ks-1, :] = 0
                if j+ks > nh:
                    window[:, :, (nh-1)-(j):ks-1] = 0
                if i < 0:
                    window[:, 0:-i-1, :] = 0
                if j < 0:
                    window[:, :, 0:-j-1] = 0
                yield window

    def iter_layers(self, layer_name):
        """
        DEPRECATED
        ----------
        Go through the layers, starting at layer layer_name
        """
        if type(layer_name) is None:
            return
        for next_layer in self.net_proto.layer:
            if layer_name == next_layer.name:
                continue
            if layer_name in next_layer.bottom:
                yield next_layer.name
                # WE NEED TO GO DEEPER
                yield from self.iter_layers(next_layer.name)

    def getLayer(self, layer_name):
        for layer in self.net_proto.layer:
            if layer.name == layer_name:
                return layer
        return None

    def addArray(self, array):
        i = len(self.dma_array)
        self.dma_array = np.concatenate((self.dma_array, array))
        return i

    def clearArrays(self):
        self.dma_array = np.zeros(shape=(0,), dtype=np.float32)

    def sendArrays(self):
        # prepare a contiguos array of data in DRAM
        print(self.dma_array.shape)
        arr = xlnk.cma_array(shape=self.dma_array.shape,dtype=np.float32)
        np.copyto(arr, self.dma_array)
        #print('{} {}'.format(arr[:], self.dma_array[:]))
        self.dma.sendchannel.stop()
        self.dma.recvchannel.stop()
        while self.dma.recvchannel.running or self.dma.sendchannel.running or self.dma.recvchannel.idle or self.dma.sendchannel.idle:
            continue
        self.dma.sendchannel.start()
        self.dma.recvchannel.start()
        while not self.dma.recvchannel.running or not self.dma.sendchannel.running:
            continue
        self.dma.sendchannel.transfer(arr)
        self.dma.recvchannel.transfer(arr)
        self.dma.sendchannel.wait()
        self.dma.recvchannel.wait()
        np.copyto(self.dma_array, arr)
        #free physical memory
        arr.freebuffer()

    def getLayerTypeInt(self, layer_type):
        layer_types = _metadata["prototxt"]["layer-types"]
        if layer_type == layer_types['Convolution'] or layer_type == layer_types['InnerProduct']:
            return 0
        elif layer_type == layer_types['Pooling']:
            return 1
        else:
            # activation funcction
            return 2

    def getActivationTypeInt(self, layer_type):
        layer_types = _metadata["prototxt"]["layer-types"]
        if layer_type == layer_types['Sigmoid']:
            return 0
        elif layer_type == layer_types['ReLU']:
            return 1
        else:
            return 2

    def getPoolTypeInt(self, pool_type):
        pool_types = _metadata["prototxt"]["pool-types"]
        if pool_type == pool_types["MAX"]:
            return 0
        if pool_type == pool_types["AVG"]:
            return 1

    def setMetadata(self, dma_meta, neurons_shape, layer, param):
        #type
        dma_meta[0]=self.getLayerTypeInt(layer.type)
        #N channels
        dma_meta[1]=neurons_shape[0]
        #N kernel, except in Pooling where it is N channels
        dma_meta[2]=param["n_kernel"]
        #Kernel size
        dma_meta[3]=param["kernel_size"]
        #Pad
        dma_meta[4]=param["pad"]
        return dma_meta

    def softmax(self, x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()

    def calculate(self):
        """
        # layer[0]: weights
        # layer[1]: biases
        for layer in self.net.params:
            W = layer[0].data
            b = layer[1].data
        """
        """
        DEPRECATED
        ----------
        # for every input layer
        for input_layer_name, neurons in self.neurons.items():
            # get next layer. We could have branching layers.. but we won't worry
            # about that at this moment
            # async for loop
            for layer_name in self.iter_layers(input_layer_name):
                layer = self.getLayer(layer_name)
                if layer is None:
                    print('[ERROR] LAYER NOT FOUND: ',layer_name)
                    continue

            . . .

        """
        np.set_printoptions(suppress=True)
        windowParam = {}
        input_layer = ""
        layer_types = _metadata["prototxt"]["layer-types"]
        self.clearArrays()
        # WE SUPPOSE THE LAYERS ARE IN ORDER IN THE .PROTOTXT
        for layer in self.net_proto.layer:
            if layer.type == layer_types["Input"]:
                input_layer = layer.name
                continue

            neurons = self.neurons[input_layer]
            output  = self.neurons[input_layer]

            print('---------------NEXT LAYER-------------------')
            print(layer)

            # Dropout type is not used in prediction phase
            # LRN type is not used in prediction phase

            if layer.type == layer_types['Convolution']:
                windowParam["kernel_size"] = layer.convolution_param.kernel_size[0]
                if len(layer.convolution_param.stride)==0:
                    windowParam["stride"] = 1
                else:
                    windowParam["stride"] = layer.convolution_param.stride[0]
                if len(layer.convolution_param.pad)==0:
                    windowParam["pad"] = 0
                else:
                    windowParam["pad"] = layer.convolution_param.pad[0]
                windowParam["n_kernel"] = layer.convolution_param.num_output

            elif layer.type == layer_types['Pooling']:
                windowParam["kernel_size"] = layer.pooling_param.kernel_size
                windowParam["stride"] = layer.pooling_param.stride
                windowParam["pad"] = 0
                # actually used for N of channels:
                windowParam["n_kernel"] = neurons.shape[0]

            elif layer.type == layer_types['ReLU']:
                pass # TODO: Continue function
            # Treat it as a convolution
            elif layer.type == layer_types['InnerProduct']:
                windowParam["kernel_size"] = neurons.shape[1]
                windowParam["stride"] = neurons.shape[1]
                windowParam["pad"] = 0
                windowParam["n_kernel"] = layer.inner_product_param.num_output

            if layer.type == layer_types['Convolution'] or layer.type == layer_types['Pooling'] or layer.type == layer_types['InnerProduct']:
                # SEND WINDOW
                # Output definition
                # (w+2*p-ks)/s + 1
                c = windowParam["n_kernel"]
                w = (neurons.shape[1]+2*windowParam["pad"]-windowParam["kernel_size"])/windowParam["stride"] +1
                h = (neurons.shape[2]+2*windowParam["pad"]-windowParam["kernel_size"])/windowParam["stride"] +1
                w = math.floor(w)
                h = math.floor(h)
                output = np.zeros(shape=(c,w,h),dtype=neurons.dtype)

                # async for loop. Send the window
                dma_window = np.zeros(
                    shape=(neurons.shape[0]*windowParam["kernel_size"]*windowParam["kernel_size"],),
                    dtype=neurons.dtype
                    )
                wi = 0
                hi = 0
                for window in self.iter_neurons(neurons, windowParam):
                    if hi==h:
                        hi=0
                        continue

                    self.clearArrays()

                    # SEND METADATA
                    dma_meta = np.zeros(shape=(5,),dtype=neurons.dtype)
                    dma_meta = self.setMetadata(dma_meta,neurons.shape,layer,windowParam)
                    self.addArray(dma_meta)

                    #(C,W,H) -> (H,W,C) so data is sent first through the channels
                    dma_window = window.swapaxes(0,-1).ravel()
                    self.addArray(dma_window)

                    if layer.type == layer_types['Pooling']:
                        # (C,)
                        pool_output = np.zeros(shape=(2+neurons.shape[0],),dtype=neurons.dtype)
                        pool_output[0] = self.getPoolTypeInt(layer.pooling_param.pool)
                        pool_output[1] = layer.pooling_param.stride
                        pi = self.addArray(pool_output)

                        # OUTPUT/CALCULATION PHASE
                        self.sendArrays()

                        # the output is at the end of the array
                        output[:,wi,hi] = self.dma_array[2+pi:]


                    elif layer.type == layer_types['Convolution'] or layer.type == layer_types['InnerProduct']:
                        # send the kernels + biases
                        blobs = self.net.params[layer.name]
                        # (N kernels, C, W, H) for Conv | (N outputs, C*W*H) for InnProd
                        kernels = blobs[0].data
                        # (N bias)
                        biases  = blobs[1].data
                        kb_size = None
                        if layer.type == layer_types['Convolution']:
                            # C * W * H + 1
                            kb_size = kernels.shape[1]*kernels.shape[2]*kernels.shape[3]+1 #+1 bcs of the bias
                        elif layer.type == layer_types['InnerProduct']:
                            # C * W * H + 1
                            kb_size = kernels.shape[1]+1 #+1 bcs of the bias
                        kernel_bias_array = np.zeros( shape=(kb_size,), dtype=neurons.dtype )
                        # for every kernel
                        for ki in range(kernels.shape[0]):
                            #(C,W,H) -> (H,W,C) so data is sent first through the channels
                            kernel_bias_array[0:-1] = kernels[ki].swapaxes(0,-1).ravel()
                            kernel_bias_array[-1] = biases[ki]
                            self.addArray(kernel_bias_array)
                        # After all the kernels
                        
                        print('SENDING WINDOW ({},{})'.format(wi,hi))

                        # OUTPUT/CALCULATION PHASE
                        self.sendArrays()
                        
                        print('SENT WINDOW ({},{})'.format(wi,hi))

                        # for every kernel
                        for ki in range(kernels.shape[0]):
                            #the result is at the end of each kernel_bias_array
                            output[kernels.shape[0]-1 -ki, wi, hi] = self.dma_array[-(ki*kb_size)-1]
                    # next window iteration
                    wi = wi+1
                    if wi>=w:
                        wi = 0
                        hi = hi+1
                # prepare neurons for next layer
                self.neurons[input_layer] = output.copy()
            elif layer.type == layer_types['Softmax']:
                output = neurons.ravel()
                output = self.softmax(output)
                oi = 0
                # (C, W, H)
                for ci in range(neurons.shape[0]):
                    for wi in range(neurons.shape[1]):
                        for hi in range(neurons.shape[2]):
                            self.neurons[input_layer][ci,wi,hi] = output[oi]
                            oi = oi+1
                pass

            else:
                # we suppose we are in an activation function layer

                self.clearArrays()

                size = neurons.shape[0]*neurons.shape[1]*neurons.shape[2]

                # SEND METADATA
                dma_meta = np.zeros(shape=(3,),dtype=neurons.dtype)
                dma_meta[0] = self.getLayerTypeInt(layer.type)
                dma_meta[1] = self.getActivationTypeInt(layer.type)
                dma_meta[2] = size
                self.addArray(dma_meta)

                # Output definition
                output = np.zeros(shape=(size,),dtype=neurons.dtype)
                output[:] = neurons.ravel() # (C, W, H)

                oi = self.addArray(output)

                # OUTPUT/CALCULATION PHASE
                self.sendArrays()

                # prepare neurons for next layer
                for i in range(neurons.shape[0]):
                    for j in range(neurons.shape[1]):
                        for k in range(neurons.shape[2]):
                            self.neurons[input_layer][i,j,k] = self.dma_array[oi]
                            oi=oi+1

            #DEBUG
            print('OUTPUT {}:'.format(self.neurons[input_layer].shape))
            print(self.neurons[input_layer][0:3])

            pass    # TODO: Continue function


# Instantiate Driver
driver = _Driver(_metadata["bitstream"])

def setModel(model_name):
    driver.init_model(model_name)

def setImages(*args):
    if driver.net is None:
        driver.init_model(_metadata["defaultmodel"])
    driver.load_input(list(args))

def calculate():
    if driver.net is None:
        raise ValueError('First set the images.')
    driver.calculate()

def result():
    if driver.net is None:
        raise ValueError('First set the images and calculate.')
    # Continue function
    return [v for v in driver.neurons.values()]

