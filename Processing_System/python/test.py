import CNN
import numpy as np
from PIL import Image

# this model needs imgs of size 224, 224
imgdata = np.array(Image.open('test_img.png').resize((224,224)))
# now the shape is (Height, Width, Channels), let's change that
imgdata = np.swapaxes(imgdata, 0, -1)

imgdata = np.zeros(shape=(1,28,28))
for i in range(0,imgdata.shape[0]):
    for j in range(0,imgdata.shape[1]):
        for k in range(0,imgdata.shape[2]):
            imgdata[i,j,k] = k+j*imgdata.shape[2]+i*imgdata.shape[1]*imgdata.shape[2]

CNN.setModel('/home/adrian/Windows/Desktop/FPGA/PYNQ_PROJECT/pynq-cnn-caffe/Processing_System/CNN/models/default.caffemodel')
CNN.setImages(imgdata)
CNN.calculate()
