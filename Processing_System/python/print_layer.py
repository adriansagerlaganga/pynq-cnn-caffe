import sys
import caffe
import numpy as np

caffe.set_mode_cpu()

def show_weights_bias(W, B):
    #                decimal digits, nº of elems, suppress small nº?
    np.set_printoptions(precision=2, threshold=7, suppress=True)
    # Number of kernels
    for ki in range(W.data.shape[0]):
        print('kernel[{:02d}] (c={:02d}, w={:02d}, h={:02d}): '.format(ki, W.data.shape[1],W.data.shape[2],W.data.shape[3]))
        # The dimensions of the kernel are W.data.shape[2] and W.data.shape[3]
        for di in range(W.data.shape[2]):
            s = ''
            # Number of channels
            for ci in range(W.data.shape[1]):
                s += '{:30s} '.format(str(W.data[ki][ci][di]))
            # Show bias
            if di==0:
                s += ' bias= {:02.2f}'.format(B.data[ki])
            print(s)


def main():
    net = caffe.Net('{:}.prototxt'.format(sys.argv[1]),
                    '{:}.caffemodel'.format(sys.argv[1]),
                    caffe.TEST)
    print('\t\t\tBLOBS')
    for lname, layer in net.blobs.items():
        print('{:50s} shape: {:}'.format(lname, layer.data.shape))
    print('\t\t\tPARAMS')
    for lname, layer in net.params.items():
        print('{:50s} weights shape: {:20s} bias shape: {:}'.format(lname, str(layer[0].data.shape), layer[1].data.shape))

    layer = list(net.params.keys())[0]
    if len(sys.argv) > 2:
        layer = sys.argv[2]
    show_weights_bias(net.params[layer][0], net.params[layer][1])

if __name__ == "__main__":
    # execute only if run as a script
    main()
    print("finished")
