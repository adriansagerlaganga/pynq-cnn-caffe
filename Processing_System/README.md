# CNN

## Table of contents

- [Requirements](#requirements)
  - [Caffe](#caffe)
- [Setup](#setup)
- [Usage](#usage)
- [Implementation](#implementation)
  - [PS-PL communication](#ps-pl-communication)

### Requirements

In this tutorial, *PYNQ Z2* runs the following Linux distribution and version:

```bash
Distributor ID: pynqlinux
Description:    PYNQ Linux, based on Ubuntu 18.04
Release:        v2.4
Codename:       provo
```

There are `508Mb` of DRAM in total. We used `1048Mb` of SD Card Swap Memory in total:

```bash
root@pynq:/home/xilinx/caffe# free --mega
              total        used        free      shared  buff/cache   available
Mem:            508         102         382           1          23         395
Swap:          1048           3        1045
```

Check you have at least the same amount of free Virtual Memory as in this example.

#### Caffe

> Caffe is a deep learning framework made with expression, speed, and modularity in mind [⁽¹⁾](https://caffe.berkeleyvision.org/).

To install *Caffe* follow this steps:

* Open a terminal using Jupyter.
* Run the commands

```bash
sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
sudo apt-get install --no-install-recommends libboost-all-dev
sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev
```

```bash
pip3 install protobuf
```

* Download Caffe:

```bash
cd /home/xilinx
git clone https://github.com/BVLC/caffe.git
```

* Copy and change `caffe/Makefile.config.example` to use CPU-only Caffe,

```bash
cd /home/xilinx/caffe
cp Makefile.config.example Makefile.config
vim Makefile.config
```

* Uncomment `CPU_ONLY := 1`
* Uncomment `USE OPENCV := 3`

* Fix some Caffe installation bugs [⁽¹⁾](https://github.com/NVIDIA/DIGITS/issues/156),

```bash
cd /home/xilinx/caffe
find . -type f -exec sed -i -e 's^"hdf5.h"^"hdf5/serial/hdf5.h"^g' -e 's^"hdf5_hl.h"^"hdf5/serial/hdf5_hl.h"^g' '{}' \;
```

```bash
cd /usr/lib/arm-linux-gnueabihf
sudo ln -s hdf5/serial/libhdf5.so libhdf5.so
sudo ln -s hdf5/serial/libhdf5_hl.so libhdf5_hl.so
```

* Clear caches

```bash
sync; echo 3 > /proc/sys/vm/drop_caches
```

* Make the code,

```bash
cd /home/xilinx/caffe
make all
make pycaffe
cmake all
cmake install
```

* Done!

### Setup

1. Create a `CNN/` folder in the `/home/xilinx/` directory of your PYNQ
2. Place the contents of this project's `CNN/` folder.
3. Create a new folder in Jupyter.
4. Place the contents of this project's `python/` folder.
> For this purpose you may need to create each file manually trhough Jupyter.

## Usage

Check the *CNN* notebook.

## Implementation

> Check [this](http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html) tutorial on CNN for good visual examples.

Calculations are done layer-by-layer.

The input of the layer is divided in ***channels***, where each *channel* is a matrix of `w x h` dimensions.
There are certain amount of filters, or ***kernels***, which are matrices with their own size. Each *kernel* has the same amount of *channels* as the input.

> ![](http://machinelearninguru.com/_images/topics/computer_vision/basics/convolutional_layer_1/rgb.gif)
>
> [http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html](http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html)

A ***weight*** is an element of a channel matrix.

A ***neuron*** are the *weights* that share the same matrix position in all the *channels*.

Since the amount of memory required for the calculations is so high, we will use a ***window*** for the input.

The *window* will have the same dimensions as the *kenrels*.

The ***bias*** is a scalar summed after every *kernel* multiplication.

#### PS-PL communication

![](http://drive.google.com/uc?export=view&id=1yzSZKdg6eGAKYd2TqbFxc98fd3f-O5P5)

The *window*, *kernel* and *bias* data is transmitted using *DMA*[⁽¹⁾](https://en.wikipedia.org/wiki/Direct_memory_access), in this order:

1. Layer Metadata:

Variable | Description
---------|------------
type     |  *CONV*, *POOL* or *RELU*
n_input*  |  Nº of channels / Input matrices  
n_kernel* |  Nº of filters / kernels / outputs
dim_kernel*| Kernel `w x h`  
pad*      |  Pad size

\[\*]: sent only in *CONV* and *POOL* layers.

Next steps depend on the type of layer

##### Convolution

2. We initialize every *neuron* in the *window*. For every *neuron*:

Variable | Description
---------|------------
N[MAX_CHANNELS]      |  Array of weights of the *neuron* <br/>It's length is the Nº of *channels*

4. We perform *neuron* multiplication. We send the *kernel* value at the position of the *neuron* and multiply for every *channel*.
5. We send the *bias*.
6. We retrieve from the PL the resulting output matrix element.
6. Repeat 3.-6. for the rest of the *kernels*.

##### Pooling

2. We initialize every *neuron* in the *window*. For every *neuron*:

Variable | Description
---------|------------
N[MAX_CHANNELS]      |  Array of weights of the *neuron* <br/>It's length is the Nº of *channels*

3. We send the *pooling type* and the *stride*.
4. We go through the window according to the *stride* and we retrieve from the PL the resulting output matrix element.

##### Activation Functions

2. We send the *function selector* and *amount of inputs*.
3. We send all the values and for each of them we retrieve the result.
